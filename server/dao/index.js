const mysql = require('mysql');
let connect;
let configuration;

/**
 * Data Access Layer
 *
 * @constructor
 * @param {Object} config - database config
 */
function DAO(config) {
  configuration = config;
  handleDisconnect();
}

function handleDisconnect() {
  connect = mysql.createConnection(configuration);
  connect.connect(function (err) {
    if (err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
    console.log("Connected to db!");
  });
  connect.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      handleDisconnect();
    } else {
      throw err;
    }
  });
}

/**
 * Return author by id from db
 * @param {int} id  - author id
 * @param {Object} responseObject - response object
 * @returns {Object} - response object
 */
DAO.prototype.getAuthorById = function (id, responseObject) {
  return new Promise(resolve =>
    connect.query('SELECT author._id,' +
      '       author.email,' +
      '       author.firstName,' +
      '       author.secondName,' +
      '       author.birthDate,' +
      '       JSON_ARRAYAGG(books.book) AS book' +
      ' FROM authors as author' +
      '  LEFT JOIN relation as books ON author._id = books.author' +
      ' WHERE author._id = ' + id +
      ' GROUP BY author.email', function (err, result) {
      if (err) {
        responseObject.errors.push("Author not exist");
        responseObject.status = 404;
        resolve(responseObject);
      } else {
        responseObject.author = result[0];
        responseObject.author.book = JSON.parse(responseObject.author.book);
        responseObject.status = 200;
        resolve(responseObject);
      }
    })
  )
    .catch(resObject => resObject);
};

/**
 * Return book by id from db
 * @param {int} id  - body id
 * @param {Object} responseObject - response object
 * @returns {Object} - response object
 */
DAO.prototype.getBookById = function (id, responseObject) {
  return new Promise(resolve =>
      connect.query('SELECT book.`_id`, ' +
        '       book.name, ' +
        '       book.publishing, ' +
        '       book.ebook, ' +
        '       book.year, ' +
        '       book.isbn, ' +
        '       book.pages, ' +
        '       JSON_ARRAYAGG(authors.author) AS author ' +
        'FROM books as book ' +
        'LEFT JOIN relation as authors ON book.`_id` = authors.book ' +
        'WHERE book.`_id` = ' + id +
        ' GROUP BY book.name', function (err, result) {
        if (err) {
          responseObject.errors.push("Book not exist");
          responseObject.status = 404;
          resolve(responseObject);
        } else {
          responseObject.book = result[0];
          responseObject.book.author = JSON.parse(responseObject.book.author);
          responseObject.status = 200;
          resolve(responseObject);
        }
      }));
};

/**
 * Return all authors from db
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.getAuthors = function (responseObject) {
  return new Promise(resolve =>
    connect.query("SELECT a._id, a.email, a.firstName,a.secondName, a.birthDate, JSON_ARRAYAGG(r.book) AS book " +
      "FROM authors a " +
      "  LEFT JOIN relation r ON a.`_id` = r.author " +
      "GROUP BY 1", function (err, result) {
      if (err || result[0] === undefined) {
        responseObject.errors.push(err);
        responseObject.status = 404;
        resolve(responseObject);
      } else {
        responseObject.authors = [];
        for (let i = 0; i < result.length; i++) {
          responseObject.authors.push(result[i]);
        }
        responseObject.status = 200;
        resolve(responseObject);
      }
    }));
};

/**
 * Return all books from db
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.getBooks = function (responseObject) {
  return new Promise(resolve =>
    connect.query("SELECT b._id, b.name, b.publishing, b.ebook, b.year, b.isbn, b.pages, JSON_ARRAYAGG(r.author) AS author" +
      " FROM books b" +
      "  LEFT JOIN relation r ON b.`_id` = r.book " +
      "GROUP BY 1", function (err, result) {
      if (err || result[0] === undefined) {
        responseObject.errors.push(err);
        responseObject.status = 400;
        resolve(responseObject);
      } else {
        responseObject.books = [];
        for (let i = 0; i < result.length; i++) {
          responseObject.books.push(result[i]);
        }
        responseObject.status = 200;
        resolve(responseObject);
      }
    }));
};

/**
 * Update specific author information by id
 * @param {int} id  - author id
 * @param {Object} body - updated information
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.updateAuthor = function (id, body, responseObject) {
  return new Promise(resolve => resolve(checkExist(id, 'Author', responseObject)))
    .then(resObject => {
      if (resObject.status === 0)
        return checkEmail(body.email, resObject);
      else
        throw resObject;
    })
    .then(resObject => {
      if (resObject.status === 0)
        responseObject = resObject;
      else
        throw resObject;
    })
    .then(() => {
      connect.query("DELETE FROM relation WHERE author = " + id);
      connect.query("UPDATE authors SET `email` = '" + body.email + "', firstName = '" +
        body.firstName + "', secondName = '" + body.secondName + "', birthDate = " +
        body.birthDate + " WHERE `_id` = " + id);
    })
    .then(() => {
        if (body.book === undefined || body.book.length === 0) {
          responseObject.status = 200;
          throw responseObject;
        }
      }
    )
    .then(() => body.book.map(book => '(' + id + ',' + book + ')').join(','))
    .then(values => connect.query("INSERT INTO relation (`author`, `book`) VALUES " + values))
    .then(() => {
      responseObject.status = 200;
      return responseObject;
    })
    .catch(resObject => resObject);
};

/**
 * Update specific book information by id
 * @param {int} id  - book id
 * @param {Object} body - updated information
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.updateBook = function (id, body, responseObject) {
  return new Promise(resolve => resolve(checkExist(id, 'Book', responseObject)))
    .then(resObject => {
      if (resObject.status === 0)
        return checkEmail(body.email, resObject);
      else
        throw resObject;
    })
    .then(resObject => {
      if (resObject.status === 0)
        responseObject = resObject;
      else
        throw resObject;
    })
    .then(() => {
      body.ebook = body.ebook === true ? 1 : 0;
      connect.query("DELETE FROM relation WHERE book = " + id);
      connect.query("UPDATE books SET `name` = '" + body.name + "', publishing = '" + body.publishing + "', ebook = '" +
        body.ebook + "', year = '" + body.year + "', isbn = '" + body.isbn + "', pages = '" + body.pages +
        "' WHERE `_id` = " + id);
    })
    .then(() => {
        if (body.author === undefined || body.author.length === 0) {
          responseObject.status = 200;
          throw responseObject;
        }
      }
    )
    .then(() => {
      return body.author.map(author => '(' + author + ',' + id + ')').join(',');
    })
    .then(values => connect.query("INSERT INTO relation (`author`, `book`) VALUES " + values))
    .then(() => {
      responseObject.status = 200;
      return responseObject;
    })
    .catch(resObject => resObject);
};

/**
 * Update some authors fields
 * @param {int} id  - author id
 * @param {Object} body - updated information
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.updateAuthorProperty = function (id, body, responseObject) {
  return new Promise(resolve => resolve(checkExist(id, 'Author', responseObject)))
    .then(resObject => {
      if (resObject.status === 0)
        return checkEmail(body.email, resObject);
      else
        throw resObject;
    })
    .then(resObject => {
      if (resObject.status === 0) {
        responseObject = resObject;
      } else
        throw resObject;
    })
    .then(
      () => Object.keys(body).map(key => " " + key + " = '" + body[key] + "'")
    )
    .then(updates => {
      connect.query("DELETE FROM relation WHERE author = " + id);
      connect.query("UPDATE authors SET" + updates + " WHERE `_id` = " + id, function (err) {
        if (err) {
          responseObject.status = 404;
          responseObject.errors.push(err);
          throw responseObject;
        }
      })
    })
    .then(() => {
      if (body.book === undefined || body.book.length === 0) {
        responseObject.status = 200;
        throw responseObject;
      }
    })
    .then(() => {
      return body.book.map(book => '(' + id + ',' + book + ')').join(',');
    })
    .then(values => connect.query("INSERT INTO relation (`author`, `book`) VALUES " + values))
    .then(() => {
      responseObject.status = 200;
      return responseObject;
    })
    .catch(resObject => resObject);
};

/**
 * Update some books fields
 * @param {int} id  - book id
 * @param {Object} body - updated information
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.updateBookProperty = function (id, body, responseObject) {
  return new Promise(resolve => resolve(checkExist(id, 'Book', responseObject)))
    .then(resObject => {
      if (resObject.status === 0)
        return checkEmail(body.email, resObject);
      else
        throw resObject;
    })
    .then(resObject => {
      if (resObject.status === 0)
        responseObject = resObject;
      else
        throw resObject;
    })
    .then(
      () => Object.keys(body).map(key => " " + key + " = '" + body[key] + "'")
    )
    .then(updates => {
      connect.query("DELETE FROM relation WHERE book = " + id);
      connect.query("UPDATE books SET" + updates + " WHERE `_id` = " + id, function (err) {
        if (err) {
          responseObject.status = 404;
          responseObject.errors.push(err);
          throw responseObject;
        }
      })
    })
    .then(() => {
      if (body.author === undefined || body.author.length === 0) {
        responseObject.status = 200;
        throw responseObject;
      }
    })
    .then(() => {
      return body.book.map(author => '(' + author + ',' + id + ')').join(',');
    })
    .then(values => connect.query("INSERT INTO relation (`author`, `book`) VALUES " + values))
    .then(() => {
      responseObject.status = 200;
      return responseObject;
    })
    .catch(resObject => resObject);
};

/**
 * Create new author in db
 * @param {Object} body - updated information
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.createNewAuthor = function (body, responseObject) {
  return new Promise(resolve => resolve(checkEmail(body.email, responseObject)))
    .then(resObject => {
      if (resObject.status === 0)
        responseObject = resObject;
      else
        throw resObject;
    })
    .then(() => {
      connect.query("INSERT INTO authors (email, firstName, secondName, birthDate) VALUES ('" + body.email + "', '" +
        body.firstName + "', '" + body.secondName + "', '" + body.birthDate + "')", function (err, result) {
        if (err) {
          responseObject.status = 404;
          responseObject.errors.push(err);
          throw responseObject;
        } else
          responseObject._id = result.insertId;
      })
    })
    .then(() => {
      if (body.book === undefined || body.book.length === 0) {
        responseObject.status = 201;
        throw responseObject;
      }
    })
    .then(() => {
      return body.book.map(book => '(' + responseObject._id + ',' + book + ')').join(',');
    })
    .then(values => connect.query("INSERT INTO relation (`author`, `book`) VALUES " + values))
    .then(() => {
      responseObject.status = 201;
      return responseObject;
    })
    .catch(resObject => resObject);
};

/**
 * Create new book in db
 * @param {Object} body - updated information
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.createNewBook = function (body, responseObject) {
  return new Promise(resolve => resolve(checkEmail(body.email, responseObject)))
    .then(resObject => {
      if (resObject.status === 0)
        responseObject = resObject;
      else
        throw resObject;
    })
    .then(() => {
      connect.query("INSERT INTO books (name, publishing, ebook, year, isbn, pages) VALUES ('" + body.name + "', '" +
        body.publishing + "', '" + body.ebook + "', '" + body.year + "', '" + body.isbn + "', '" + body.pages + "')",
        function (err, result) {
          if (err) {
            responseObject.status = 404;
            responseObject.errors.push(err);
            throw responseObject;
          } else
            responseObject._id = result.insertId;
        })
    })
    .then(() => {
      if (body.author === undefined || body.author.length === 0) {
        responseObject.status = 201;
        throw responseObject;
      }
    })
    .then(() => {
      return body.author.map(author => '(' + author + ',' + responseObject._id + ')').join(',');
    })
    .then(values => connect.query("INSERT INTO relation (`author`, `book`) VALUES " + values))
    .then(() => {
      responseObject.status = 201;
      return responseObject;
    })
    .catch(resObject => resObject);
};

/**
 * Remove author from db
 * @param {int} id  - author id
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.removeAuthor = function (id, responseObject) {
  return new Promise(resolve => resolve(checkExist(id, 'Author', responseObject)))
    .then((resObject) => {
        if (resObject.status === 0)
          responseObject = resObject;
        else
          throw resObject;
      }
    )
    .then(() => connect.query("DELETE FROM relation WHERE author = " + id, function (err) {
      if (err) {
        responseObject.status = 404;
        responseObject.errors.push(err);
        throw responseObject;
      }
    }))
    .then(() => connect.query("DELETE FROM authors WHERE _id = " + id, function (err) {
      if (err) {
        responseObject.status = 404;
        responseObject.errors.push(err);
        throw responseObject;
      }
    }))
    .then(() => {
      responseObject.status = 200;
      return responseObject
    })
    .catch(resObject => resObject);
};

/**
 * Remove book from db
 * @param {int} id  - book id
 * @param {Object} responseObject - response Object
 * @returns {Object} - response object
 */
DAO.prototype.removeBook = function (id, responseObject) {
  return new Promise(resolve => resolve(checkExist(id, 'Book', responseObject)))
    .then((resObject) => {
        if (resObject.status === 0)
          responseObject = resObject;
        else
          throw resObject;
      }
    )
    .then(() => connect.query("DELETE FROM relation WHERE book = " + id, function (err) {
      if (err) {
        responseObject.status = 404;
        responseObject.errors.push(err);
        throw responseObject;
      }
    }))
    .then(() => connect.query("DELETE FROM books WHERE _id = " + id, function (err) {
      if (err) {
        responseObject.status = 404;
        responseObject.errors.push(err);
        throw responseObject;
      }
    }))
    .then(() => {
      responseObject.status = 200;
      return responseObject
    })
    .catch(resObject => resObject);
};

/**
 * Check if email field is not empty
 * @param email - email
 * @param responseObject - response Object
 * @returns {Object} - response Object
 */
function checkEmail(email, responseObject) {
  if (email === undefined || email === '') {
    responseObject.errors.push('E-mail is require');
    responseObject.status = 400;
  }
  return responseObject;
}

/**
 * Check if author or book exist in db
 * @param id - author or book id
 * @param type - Indicates the author is or a book
 * @param responseObject - response Object
 * @returns {Promise<any>} - response Object
 */
function checkExist(id, type, responseObject) {
  return new Promise(resolve =>
    connect.query("SELECT * FROM " + type.toLowerCase() + 's' + " WHERE _id = " + id, function (err, result) {
      if (err || result[0] === undefined) {
        responseObject.errors.push(type + ' not exist');
        responseObject.status = 404;
      }
      resolve(responseObject);
    }))
}

/**
 * Create database instance and load init data
 * @param {Object} data - init database data
 * @param {Function} callback - two params err, callback result
 * @returns {void}
 */
DAO.prototype.init = function (data, callback) {
  //TODO create instance and load data
  callback && callback();
};

/**
 * Clear database
 * @param {Function} callback - two params err, callback result
 * @returns {void}
 */
DAO.prototype.clear = function (callback) {
  //TODO clear database
  callback && callback();
};

module.exports = DAO;