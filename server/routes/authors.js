'use strict';

const express = require('express');
const controller = require('../controllers/authors');

const router = express.Router();

router.get('/:id', controller.getAuthorById);
router.get('/', controller.getAuthors);
router.put('/:id', controller.updateAuthor);
router.patch('/:id', controller.updateAuthorProperty);
router.post('/', controller.createNewAuthor);
router.delete('/:id', controller.removeAuthor);

module.exports = router;

