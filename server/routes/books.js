'use strict';

const express = require('express');
const controller = require('../controllers/books');

const router = express.Router();

router.get('/:id', controller.getBookById);
router.get('/', controller.getBooks);
router.put('/:id', controller.updateBook);
router.patch('/:id', controller.updateBookProperty);
router.post('/', controller.createNewBook);
router.delete('/:id', controller.removeBook);

module.exports = router;