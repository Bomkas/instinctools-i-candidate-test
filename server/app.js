'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const DAO = require('./dao');
const authors = require('./routes/authors');
const books = require('./routes/books');

const PORT = 3000;

const app = express();
let dao = new DAO(
  {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'library',
    port: 3306
  }
);

/**
 * Middleware
 */
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../client')));

/**
 * Routes
 */
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/index.html'));
});
app.use('/api/authors', authors);
app.use('/api/books', books);

/**
 * Init database
 */
/*dao.init({/!*init data*!/}, (err, db) => {

  if (err) {
    console.error(err);
  }
});*/

/**
 * Start app
 */
app.listen(PORT, function () {
  console.log(`App listening on port ${PORT}!`);
});

module.exports.app = app;
module.exports.dao = dao;
