'use strict';
const dao = require("../app");
const deasync = require("deasync");
/**
 * Send specific book entity by id
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
function getBookById(req, res) {
  let send = getBook(req.params.id);
  res.status(send.status).send(send);
}

/**
 * Send all books information
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
function getBooks(req, res) {
  let send = (dao.dao.getBooks(Object.assign({}, responseObject)));
  res.status(send.status).send(send);
}

/**
 * Update specific book information by id
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
function updateBook(req, res) {
  let send = dao.dao.updateBook(req.params.id, req.body, Object.assign({}, responseObject));
  while (send.status === undefined) {
    deasync.runLoopOnce();
  }
  send.status === 200 ? res.status(send.status).send(getBook(req.params.id)) : res.status(send.status).send(send);
}

/**
 * Update some books fields
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
function updateBookProperty(req, res) {
  let send = dao.dao.updateBookProperty(req.params.id, req.body, Object.assign({}, responseObject));
  send.status === 200 ? res.status(send.status).send(getBook(req.params.id)) : res.status(send.status).send(send);
}

/**
 * Create new book in db
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
function createNewBook(req, res) {
  let send = dao.dao.createNewBook(req.body, Object.assign({}, responseObject));
  send.status === 201 ? res.status(send.status).send(getBook(send._id)) : res.status(send.status).send(send);
}

/**
 * Remove book from db
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
function removeBook(req, res) {
  let send = dao.dao.removeBook(req.params.id, Object.assign({}, responseObject));
  //send.status = send.status === 200 ? send.status = 'OK' : send.status;
  send.status === 200 ? res.status(send.status).send(send) : res.status(send.status).send(send);
}

/**
 * Get book from db
 * @param {int} id  - author id
 * @return {Object} - author info
 */
function getBook(id) {
  return dao.dao.getBookById(id, Object.assign({}, responseObject));
}

/**
 * Response object template
 * @type {{status: int, errors: Array}}
 */
let responseObject = {
  status: 0,
  errors: []
};

module.exports.getBookById = getBookById;
module.exports.updateBook = updateBook;
module.exports.getBooks = getBooks;
module.exports.updateBookProperty = updateBookProperty;
module.exports.createNewBook = createNewBook;
module.exports.removeBook = removeBook;