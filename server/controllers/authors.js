'use strict';
const dao = require("../app");

/**
 * Send specific author entity by id
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
let getAuthorById = function getAuthorById(req, res) {
  new Promise(resolve => resolve(getAuthor(req.params.id)))
    .then(send => res.status(send.status).send(send));
};

/**
 * Send all authors information
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
let getAuthors = function getAuthors(req, res) {
  new Promise(resolve =>
    resolve(dao.dao.getAuthors(Object.assign({}, responseObject))))
    .then(send =>
      res.status(send.status).send(send));
};

/**
 * Update specific author information by id
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
let updateAuthor = function updateAuthor(req, res) {
  new Promise(resolve =>
    resolve(dao.dao.updateAuthor(req.params.id, req.body, Object.assign({}, responseObject))))
    .then(send =>
      send.status === 200 ? res.status(send.status).send(getAuthor(req.params.id)) : res.status(send.status).send(send));
};

/**
 * Remove author from db
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
let removeAuthor = function removeAuthor(req, res) {
  new Promise(resolve =>
    resolve(dao.dao.removeAuthor(req.params.id, Object.assign({}, responseObject))))
    .then(send =>
      send.status === 200 ? res.status(send.status).send(send) : res.status(send.status).send(send));
};

/**
 * Update some authors fields
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
let updateAuthorProperty = function updateAuthorProperty(req, res) {
  new Promise(resolve =>
    resolve(dao.dao.updateAuthorProperty(req.params.id, req.body, Object.assign({}, responseObject))))
    .then(send =>
      send.status === 200 ? res.status(send.status).send(getAuthor(req.params.id)) : res.status(send.status).send(send));
};

/**
 * Create new author in db
 * @param {Object} req - HTTP request object
 * @param {Object} res - HTTP response object
 * @returns {void}
 */
let createNewAuthor = function createNewAuthor(req, res) {
  new Promise(resolve =>
    resolve(dao.dao.createNewAuthor(req.body, Object.assign({}, responseObject))))
    .then(send =>
      send.status === 201 ? res.status(send.status).send(getAuthor(send._id)) : res.status(send.status).send(send));
};

/**
 * Response object template
 * @type {{status: int, errors: Array}}
 */
const responseObject = {
  status: 0,
  errors: []
};

/**
 * Get author from db
 * @param {int} id  - author id
 * @return {Object} - author info
 */
function getAuthor(id) {
  return dao.dao.getAuthorById(id, Object.assign({}, responseObject));
}

module.exports.getAuthorById = getAuthorById;
module.exports.getAuthors = getAuthors;
module.exports.updateAuthor = updateAuthor;
module.exports.removeAuthor = removeAuthor;
module.exports.updateAuthorProperty = updateAuthorProperty;
module.exports.createNewAuthor = createNewAuthor;