function addAuthors(body, authors) {
  body.author = authors !== [] ? authors.map(author => author.value) : [];
}

function removeEmptyFields(body) {
  Object.keys(body).forEach(key => {
    if (body[key] === '' || body[key] === undefined)
      delete body[key];
  });
}

function replaceQuotes(body) {
  Object.keys(body).forEach(e => body[e] = body[e].replace(/\'/gi, "\\'"));
}

function addBooks(body, books) {
  body.book = books !== [] ? books.map(book => book.value) : [];
}

export {addAuthors, removeEmptyFields, replaceQuotes, addBooks}