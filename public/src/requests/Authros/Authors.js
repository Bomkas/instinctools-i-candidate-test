import {addBooks, removeEmptyFields, replaceQuotes} from "../utility";

const API = "/api/authors";

function getAuthorById(id) {
  return fetch(`${API}/${id}`, {
    method: 'get'
  })
    .then(msg => msg.json());
}

function getAuthorsCollection() {
  return fetch(`${API}/`, {
    method: 'get'
  })
    .then(msg => msg.json())
    .then(msg => {
      msg.authors.forEach(e => {
        e.book = JSON.parse(e.book)
      });
      return msg;
    });
}

function updateAuthor(id, body, books) {
  replaceQuotes(body);
  addBooks(body, books);

  return fetch(`${API}/${id}`, {
    method: 'put',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'}
  })
    .then(msg => msg.json());
}

function updateAuthorFields(id, body, books) {
  replaceQuotes(body);
  addBooks(body, books);
  removeEmptyFields(body);

  return fetch(`${API}/${id}`, {
    method: 'PATCH',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'}
  })
    .then(msg => msg.json());
}

function createAuthor(body, books) {
  replaceQuotes(body);
  addBooks(body, books);

  return fetch(`${API}/`, {
    method: 'post',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'}
  })
    .then(msg => msg.json());
}

function deleteAuthor(id) {
  return fetch(`${API}/${id}`, {
    method: 'delete'
  })
    .then(msg => msg.json());
}

export {getAuthorById, getAuthorsCollection, createAuthor, deleteAuthor, updateAuthor, updateAuthorFields}