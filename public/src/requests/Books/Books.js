import {addAuthors, removeEmptyFields, replaceQuotes} from "../utility";

const API = "/api/books";

function getBookById(id) {
  return fetch(`${API}/${id}`, {
    method: 'get'
  })
    .then(msg => msg.json())
}

function getBooks() {
  return fetch(`${API}/`, {
    method: 'get'
  })
    .then(msg => msg.json())
    .then(msg => {
      msg.books.forEach(e => {
        e.author = JSON.parse(e.author);
      });
      return msg;
    })
}

function updateBook(id, body, authors) {
  replaceQuotes(body);
  addAuthors(body, authors);

  return fetch(`${API}/${id}`, {
    method: 'put',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'}
  })
    .then(msg => msg.json());
}

function updateBookFields(id, body, authors) {
  replaceQuotes(body);
  addAuthors(body, authors);
  removeEmptyFields(body);

  return fetch(`${API}/${id}`, {
    method: 'PATCH',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'}
  })
    .then(msg => msg.json());
}

function createBook(body, authors) {
  replaceQuotes(body);
  addAuthors(body, authors);

  return fetch(`${API}/`, {
    method: 'post',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'}
  })
    .then(msg => msg.json());
}

function deleteBook(id) {
  return fetch(`${API}/${id}`, {
    method: 'delete'
  })
    .then(msg => msg.json());
}

export {getBookById, createBook, deleteBook, getBooks, updateBook, updateBookFields};