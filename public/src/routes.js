export const root = '/';
export const index = 'index.html';
export const books = '/books';
export const authors = '/authors';