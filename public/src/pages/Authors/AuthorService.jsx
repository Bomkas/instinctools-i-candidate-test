import * as request from "../../requests/Authros/Authors";
import headers from "./extra/headers";
import TableResults from "../../components/TableResults";
import React from "react";

function handleClose() {
  this.setState({show: false, response: ''});
}

function handleShow(title, display = 'none', inputs = 'none', method = null, inputMethod = this.getAuthorById) {
  this.setState({
    show: true,
    modalTitle: title,
    displayInput: display === 'none' ? 'library-display-none' : 'library-display-block library-margin-10px',
    displayInputs: inputs === 'none' ? 'library-display-none' : 'library-display-block',
    method: method,
    inputMethod: inputMethod
  });
}

function handleChange(id, value) {
  this.setState({id: value.replace(/^0+[^\\d\\$,]/, '')});
}

function optionChange(selectedOption) {
  this.setState({selectedOption: selectedOption});
}

function getAuthorById(event) {
  request.getAuthorById(this.state.id).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.author]}/> : response.errors
  }));
  event.preventDefault();
}

function getAuthorsCollection() {
  request.getAuthorsCollection().then(response => this.setState({
    response: response.status === 200 ? <TableResults headers={headers} results={response.authors}/> : response.errors
  }));
}

function updateAuthor(body) {
  request.updateAuthor(this.state.id, body, this.state.selectedOption).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.author]}/> : response.errors
  }))
}

function createAuthor(body) {
  request.createAuthor(body, this.state.selectedOption).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.author]}/> : response.errors
  }))
}

function updateAuthorFields(body) {
  request.updateAuthorFields(this.state.id, body, this.state.selectedOption).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.author]}/> : response.errors
  }))
}

function deleteAuthor() {
  request.deleteAuthor(this.state.id).then(response => this.setState({
    response: response.status === 200 ? 'Author successfully delete' : response.errors
  }))
}

export {
  handleChange,
  updateAuthorFields,
  updateAuthor,
  deleteAuthor,
  createAuthor,
  getAuthorsCollection,
  getAuthorById,
  handleClose,
  handleShow,
  optionChange
};