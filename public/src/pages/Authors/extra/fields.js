const fields = [
  {
    id: 'email',
    label: 'E-mail',
    type: 'text'
  },
  {
    id: 'firstName',
    label: 'First Name',
    type: 'text'
  },
  {
    id: 'secondName',
    label: 'Second Name',
    type: 'text'
  },
  {
    id: 'birthDate',
    label: 'Birth Date',
    type: 'date'
  }
];

export default fields;