import React from 'react';
import Select from 'react-select';
import Layout from "../../components/Layout/Layout";
import {Button, Modal, ButtonGroup, ControlLabel} from "react-bootstrap";
import Field from "../../components/Field";
import Form from "../../components/Form";
import fields from "./extra/fields";
import {
  handleChange,
  updateAuthorFields,
  updateAuthor,
  deleteAuthor,
  createAuthor,
  getAuthorsCollection,
  getAuthorById,
  handleClose,
  handleShow,
  optionChange
} from "./AuthorService";
import {getBooks} from "../../requests/Books/Books";

class AuthorContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = handleShow.bind(this);
    this.handleClose = handleClose.bind(this);
    this.handleChange = handleChange.bind(this);
    this.getAuthorById = getAuthorById.bind(this);
    this.getAuthorsCollection = getAuthorsCollection.bind(this);
    this.updateAuthor = updateAuthor.bind(this);
    this.updateAuthorFields = updateAuthorFields.bind(this);
    this.optionChange = optionChange.bind(this);
    this.createAuthor = createAuthor.bind(this);
    this.deleteAuthor = deleteAuthor.bind(this);

    this.state = {
      show: false,
      response: null,
      id: '',
      modalTitle: '',
      displayInput: null,
      displayInputs: null,
      fields: '',
      options: null,
      selectedOption: [],
      method: null,
      inputMethod: null
    };
  }

  componentWillMount() {
    getBooks()
      .then(books => {
        if (books.status === 200) {
          let onlyNames = [];
          books.books.forEach(book => {
            onlyNames.push({value: book._id, label: book.name});
          });
          return (onlyNames);
        } else
          throw books.errors;
      })
      .then(options => this.setState({options: options}))
      .catch(error => console.log(error))
  }

  render() {
    const {selectedOption} = this.state;

    return (
      <Layout>
        <ButtonGroup>
          <Button onClick={() => this.handleShow('Author by id', 'block')}>
            Get author by id
          </Button>
          <Button
            onClick={() => {
              this.handleShow('Authors collection');
              this.getAuthorsCollection();
            }}>
            Get authors collection
          </Button>
          <Button
            onClick={() => this.handleShow('Update Author Information', 'block', 'block',
              this.updateAuthor)}
          >
            Update Author Information
          </Button>
          <Button
            onClick={() => this.handleShow('Update Author Fields', 'block', 'block',
              this.updateAuthorFields)}
          >
            Update Author Fields
          </Button>
          <Button
            onClick={() => this.handleShow('Create new Author', 'none', 'block', this.createAuthor)}
          >
            Create new Author
          </Button>
          <Button
            onClick={() => this.handleShow('Delete Author', 'block', 'none', null,
              this.deleteAuthor)}
          >
            Delete Author
          </Button>
        </ButtonGroup>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.state.modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className={this.state.displayInput}>
              <ControlLabel>ID</ControlLabel>
              <Field type="number" updateField={this.handleChange} value={this.state.id} placeholder="id"/>
              <Button onClick={this.state.inputMethod}>
                Show
              </Button>
              <hr/>
            </div>
            <div className={this.state.displayInputs}>
              <Form updateFields={this.state.method} fields={fields}>
                <ControlLabel>Books</ControlLabel>
                <Select
                  isMulti={true}
                  value={selectedOption}
                  onChange={this.optionChange}
                  options={this.state.options}
                />
              </Form>
              {this.state.select}
            </div>
            {this.state.response}
          </Modal.Body>
        </Modal>
      </Layout>
    );
  }
}

export default AuthorContainer;