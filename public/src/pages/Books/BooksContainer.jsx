import React from 'react';
import Select from 'react-select';
import Layout from "../../components/Layout/Layout";
import {Button, ButtonGroup, ControlLabel, Modal} from "react-bootstrap";
import Form from "../../components/Form";
import fields from "./extra/fields";
import Field from "../../components/Field";
import {getAuthorsCollection} from "../../requests/Authros/Authors";
import {
  getBookById,
  optionChange,
  handleShow,
  handleClose,
  handleChange,
  updateBookFields,
  updateBook,
  deleteBook,
  createBook,
  getBooksCollection
} from "./BooksService";

class BooksContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleClose = handleClose.bind(this);
    this.handleShow = handleShow.bind(this);
    this.handleChange = handleChange.bind(this);
    this.getBookById = getBookById.bind(this);
    this.getBooksCollection = getBooksCollection.bind(this);
    this.updateBook = updateBook.bind(this);
    this.optionChange = optionChange.bind(this);
    this.updateBookFields = updateBookFields.bind(this);
    this.createBook = createBook.bind(this);
    this.deleteBook = deleteBook.bind(this);

    this.state = {
      show: false,
      response: null,
      id: '',
      modalTitle: '',
      displayInput: null,
      displayInputs: null,
      fields: '',
      options: null,
      selectedOption: [],
      method: null,
      inputMethod: null
    };
  }

  componentWillMount() {
    getAuthorsCollection()
      .then(authors => {
        if (authors.status === 200) {
          let onlyNames = [];
          authors.authors.forEach(author => {
            onlyNames.push({value: author._id, label: author.firstName + ' ' + author.secondName});
          });
          return (onlyNames);
        } else
          throw authors.errors;
      })
      .then(options => this.setState({options: options}))
      .catch(error => console.log(error))
  }

  render() {
    const {selectedOption} = this.state;

    return (
      <Layout>
        <ButtonGroup>
          <Button
            onClick={() => this.handleShow('Book by id', 'block')}
          >
            Book by id
          </Button>
          <Button
            onClick={() => {
              this.handleShow('Books Collection');
              this.getBooksCollection();
            }}>
            Get Books Collection
          </Button>
          <Button
            onClick={() => this.handleShow('Update Book Information', 'block', 'block',
              this.updateBook)}
          >
            Update Book Information
          </Button>
          <Button
            onClick={() => this.handleShow('Update Book Fields', 'block', 'block',
              this.updateBookFields)}
          >
            Update Book Fields
          </Button>
          <Button
            onClick={() => this.handleShow('Create new Book', 'none', 'block', this.createBook)}
          >
            Create new Book
          </Button>
          <Button
            onClick={() => this.handleShow('Delete Book', 'block', 'none', null,
              this.deleteBook)}
          >
            Delete Book
          </Button>
        </ButtonGroup>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.state.modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className={this.state.displayInput}>
              <ControlLabel>ID</ControlLabel>
              <Field type="number" updateField={this.handleChange} value={this.state.id} placeholder="id"/>
              <Button onClick={this.state.inputMethod}>
                Show
              </Button>
              <hr/>
            </div>
            <div className={this.state.displayInputs}>
              <Form updateFields={this.state.method} fields={fields}>
                <ControlLabel>Authors</ControlLabel>
                <Select
                  isMulti={true}
                  value={selectedOption}
                  onChange={this.optionChange}
                  options={this.state.options}
                />
              </Form>
              {this.state.select}
            </div>
            {this.state.response}
          </Modal.Body>
        </Modal>
      </Layout>
    );
  }
}

export default BooksContainer;