import * as request from "../../requests/Books/Books";
import headers from "./extra/headers";
import TableResults from "../../components/TableResults";
import React from "react";

function handleClose() {
  this.setState({show: false, response: ''});
}

function handleShow(title, display = 'none', inputs = 'none', method = null, inputMethod = this.getBookById) {
  this.setState({
    show: true,
    modalTitle: title,
    displayInput: display === 'none' ? 'library-display-none' : 'library-display-block library-margin-10px',
    displayInputs: inputs === 'none' ? 'library-display-none' : 'library-display-block',
    method: method,
    inputMethod: inputMethod
  });
}

function handleChange(id, value) {
  this.setState({id: value.replace(/^0+[^\\d\\$,]/, '')});
}

function optionChange(selectedOption) {
  this.setState({selectedOption: selectedOption});
}

function getBookById(event) {
  request.getBookById(this.state.id).then(response => this.setState({
    response: response.status === 200 ? <TableResults headers={headers} results={[response.book]}/> : response.errors
  }));
  event.preventDefault();
}

function getBooksCollection() {
  request.getBooks().then(response => this.setState({
    response: response.status === 200 ? <TableResults headers={headers} results={response.books}/> : response.errors
  }));
}

function updateBook(body) {
  request.updateBook(this.state.id, body, this.state.selectedOption).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.book]}/> : response.errors
  }))
}

function updateBookFields(body) {
  request.updateBookFields(this.state.id, body, this.state.selectedOption).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.book]}/> : response.errors
  }))
}

function createBook(body) {
  request.createBook(body, this.state.selectedOption).then(response => this.setState({
    response: response.status === 200 ?
      <TableResults headers={headers} results={[response.book]}/> : response.errors
  }))
}

function deleteBook() {
  request.deleteBook(this.state.id).then(response => this.setState({
    response: response.status === 200 ? 'Book successfully delete' : response.errors
  }))
}

export {
  getBookById,
  optionChange,
  handleShow,
  handleClose,
  handleChange,
  updateBookFields,
  updateBook,
  deleteBook,
  createBook,
  getBooksCollection
}