const headers = [
  'ID',
  'Name',
  'Publishing',
  'E-Book',
  'Publication Year',
  'ISBN',
  'Pages',
  'Authors'
];

export default headers;