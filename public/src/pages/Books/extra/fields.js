const fields = [
  {
    id: 'name',
    label: 'Name',
    type: 'text'
  },
  {
    id: 'publishing',
    label: 'Publishing',
    type: "text"
  },
  {
    id: 'ebook',
    label: 'E-Book',
    type: "text"
  },
  {
    id: 'year',
    label: 'Publication Year',
    type: 'text'
  },
  {
    id: 'isbn',
    label: 'ISBN',
    type: 'text'
  },
  {
    id: 'pages',
    label: 'Pages',
    type: 'text'
  }
];

export default fields;