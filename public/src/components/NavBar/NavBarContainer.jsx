import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {Link} from "react-router";
import {books, authors} from "../../routes";

class NavBarContainer extends React.Component {
  render() {
    return (
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem>
              <Link
                to={authors}
                onlyActiveOnIndex
              >
                Authors
              </Link>
            </NavItem>
            <NavItem>
              <Link
                to={books}
                onlyActiveOnIndex
              >
                Books
              </Link>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default NavBarContainer;