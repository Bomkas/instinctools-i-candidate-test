import React from 'react';
import {getBookById} from "../../../requests/Books/Books";
import {getAuthorById} from "../../../requests/Authros/Authors";

class TableStroke extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      result: this.props.result,
      Ids: this.props.result.book || this.props.result.author,
      out: ''
    };

    if (this.props.result.book !== undefined) {
      this.state.method = getBookById;
      this.state.type = TableStroke.getBookName;
    } else {
      this.state.method = getAuthorById;
      this.state.type = TableStroke.getAuthorName;
    }
  }

  componentWillMount() {
    const {Ids} = this.state;

    Ids[0] !== undefined && Ids[0] !== null ? Ids.forEach(id =>
      this.state.method(id).then(response => response.status === 200 ? this.setState(prevState => {
          return {out: prevState.out + this.state.type(response) + ', '};
        })
        : this.setState({out: response.errors}))
    ) : this.setState({out: ''})
  }

  static getBookName(response) {
    return response.book.name;
  }

  static getAuthorName(response) {
    return response.author.firstName.concat(' ', response.author.secondName);
  }

  render() {
    const {result} = this.state;
    return (
      <tr>
        {Object.keys(result).map(
          (key, index) =>
            <td key={index}>{key === 'book' || key === 'author' ? this.state.out.slice(0, -2) : result[key]}</td>
        )}
      </tr>
    )
  }
}

export default TableStroke;