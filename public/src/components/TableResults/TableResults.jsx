import React from "react";
import {Table} from "react-bootstrap";
import TableHead from "../TableHead";
import TableStroke from "./TableStroke";

const TableResults = ({headers, results}) => (
  <Table size="sm">
    <TableHead headers={headers}/>
    <tbody>
    {results.map(
      (result, index) => <TableStroke key={index} result={result}/>
    )}
    </tbody>
  </Table>
);

export default TableResults;