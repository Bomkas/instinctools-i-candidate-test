import React from 'react';
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";

class Field extends React.Component {

  render() {
    return (
      <FormGroup controlId={this.props.id}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl inputRef={ref => this.input = ref} type={this.props.type}
                     onChange={() => this.props.updateField(this.props.id, this.input.value)}/>
      </FormGroup>
    )
  }
}

export default Field;