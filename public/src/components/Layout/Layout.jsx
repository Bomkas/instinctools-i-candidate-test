import NavBar from "../NavBar";
import React from "react";
import {Col, Grid, Row} from "react-bootstrap";


const Layout = ({children}) => (
  <Grid fluid>
    <Row>
      <Col>
        <NavBar/>
      </Col>
      <Col>
        <div className="layout">
        {children}
        </div>
      </Col>
    </Row>
  </Grid>
);

export default Layout;