import React from 'react';
import Field from "../Field";
import {Button} from "react-bootstrap";

class Form extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.updateField = this.updateField.bind(this);

    this.state = {};
    this.props.fields.forEach(field => this.state[field.id] = '');
  }

  updateField(id, value) {
    this.setState({
      [id]: value
    })
  }

  render() {
    return (
      <div>
        {this.props.fields.map(field =>
          <Field updateField={this.updateField} key={field.id} id={field.id} label={field.label} type={field.type}
                 placeholder={field.placeholder}/>)}
        {this.props.children}
        <Button className="library-submit-button" onClick={() => this.props.updateFields(this.state)}>Submit</Button>
      </div>
    )
  }
}

export default Form;