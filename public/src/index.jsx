import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import * as routes from './routes';
import Books from './pages/Books';
import Authors from './pages/Authors';

require('es6-promise').polyfill();

ReactDOM.render(
    <Router history={browserHistory}>
      <Route path={routes.root}>
        <IndexRoute component={Authors}/>
        <Route path={routes.index} component={Authors}/>
        <Route path={routes.books} component={Books}/>
        <Route path={routes.authors} component={Authors}/>
      </Route>
    </Router>,
  document.getElementById('app')
);