-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 26 2018 г., 13:39
-- Версия сервера: 5.6.41
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `library`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `_id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `secondName` varchar(255) DEFAULT NULL,
  `birthDate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`_id`, `email`, `firstName`, `secondName`, `birthDate`) VALUES
(1, 'au1@authorEvents.com', 'A 1', 'A 1', 1),
(2, 'au2@authorEvents.com', 'A 2', 'A 2', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `publishing` varchar(255) DEFAULT NULL,
  `ebook` tinyint(1) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `pages` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`_id`, `name`, `publishing`, `ebook`, `year`, `isbn`, `pages`) VALUES
(1, 'Book 1', 'O\'Reilly', 1, 2016, '123-345-678', 657),
(2, 'Book 2', 'Williams', 0, 2015, '321-543-876', 300);

-- --------------------------------------------------------

--
-- Структура таблицы `relation`
--

CREATE TABLE `relation` (
  `author` int(11) DEFAULT NULL,
  `book` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `relation`
--

INSERT INTO `relation` (`author`, `book`) VALUES
(1, 1),
(1, 2),
(2, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`_id`),
  ADD UNIQUE KEY `authors_id_uindex` (`_id`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`_id`),
  ADD UNIQUE KEY `books_id_uindex` (`_id`);

--
-- Индексы таблицы `relation`
--
ALTER TABLE `relation`
  ADD KEY `book` (`book`),
  ADD KEY `author` (`author`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `relation`
--
ALTER TABLE `relation`
  ADD CONSTRAINT `author` FOREIGN KEY (`author`) REFERENCES `authors` (`_id`),
  ADD CONSTRAINT `book` FOREIGN KEY (`book`) REFERENCES `books` (`_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
