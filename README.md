# *itools Full Stack Test
![logo](https://bytebucket.org/archik/itools-candidate-test/raw/53dd1b388d2218b67a49ba18512393f3657d3e9e/media/logo.png "logo")


Please, run authorEvents via command
```
node itools-candidate-authorEvents
```
or
```
node itools-candidate-authorEvents/index.js
```
And perform all the tasks step by step

## Step 1. Complete authorEvents (RUS language)

![s1](https://bytebucket.org/archik/itools-candidate-test/raw/53dd1b388d2218b67a49ba18512393f3657d3e9e/media/s1.png "s1")

## Step 2. Complete server side part of application

![s2](https://bytebucket.org/archik/itools-candidate-test/raw/53dd1b388d2218b67a49ba18512393f3657d3e9e/media/s2.png "s2")

## Step 3. Complete client side part of application

![s3](https://bytebucket.org/archik/itools-candidate-test/raw/53dd1b388d2218b67a49ba18512393f3657d3e9e/media/s3.png "s3")

## Step 4. Publish your results

![s4](https://bytebucket.org/archik/itools-candidate-test/raw/d99a8e7bb728c1fb2aa91fc9516e01c5e5f94f0c/media/s4.png "s4")


You can run specific authorEvents step via command
```
node itools-candidate-authorEvents --step 2
```
or short form
```
node itools-candidate-authorEvents -s 2
```

## License
Copyright (c) 2016 archik
Licensed under the MIT license.