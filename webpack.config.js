/* eslint-disable import/no-extraneous-dependencies,  no-var */
const webpack = require('webpack');
const path = require('path');
const VendorChunkPlugin = require('webpack-vendor-chunk-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

let webpackConfig;

const NODE_ENV = process.env.NODE_ENV || 'development';

webpackConfig = {
  context: __dirname,
  node: {
    net: 'empty',
    tls: 'empty',
    dns: 'empty'
  },
  entry: {
    app: './public/src/index.jsx',
    vendor: [
      'es6-promise',
      'react',
      'react-bootstrap',
      'react-dom',
      'react-router',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'client'),
    filename: 'index.js',
    publicPath: '/assets/',
  },
  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js', '.jsx'],
  },
  resolveLoader: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.webpack-loader.js', '.web-loader.js', '.loader.js', '.js'],
  },
  devtool: NODE_ENV === 'development' ? 'source-map' : null,
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          plugins: ['transform-runtime'],
          presets: ['es2015', 'react'],
        },
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ],
  },
  plugins: [
    new CleanWebpackPlugin([path.resolve(__dirname, 'client')], {
      root: path.resolve(__dirname, 'client'),
    }),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'),
    new VendorChunkPlugin('vendor'),
    new CopyWebpackPlugin([
      {from: './public/src/index.html', to: 'index.html'},
      {from: './public/css', to: 'css'},
      {from: './public/assets', to: 'assets'},
      {from: './public/fonts', to: 'fonts'},
      {from: './public/favicon.ico', to: 'favicon.ico'}
    ]),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(NODE_ENV),
      },
    }),
  ],
};

if (NODE_ENV === 'production') {
  webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({}));
}

module.exports = webpackConfig;